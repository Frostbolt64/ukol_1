import * as redis from "redis";
import * as requestPromise from "request-promise";
import {Server} from "./server";

/*
* Tento soubor reprezentuje vstupní bod aplikace.
* Demonstrativně je zde odeslán HTTP POST požadavek na router /track a jeho 'count' hodnota.
* V rámci reálné aplikace by toto bylo realizováno formulářem na front-endu, atp.
*
* DŮLEŽITÉ: Redis databáze musí být spuštěna. Jinak aplikace nebude spuštěna.
*
*/

const server: Server = Server.getInstance();
const redisClient: redis.RedisClient = redis.createClient();
redisClient.on("error", (error: any): void => {
    console.log("Nelze se připojit k Redis databázi. Zkontrolujte, zdali je databáze spuštěna.");
    process.exit(1);
});
redisClient.on("connect", (): void => {
    server.runServer();
    redisClient.quit();
    sendExamplePostRequest();
});

/**
 * Odešle vzorový HTTP POST požadavek na router /track.
 */
function sendExamplePostRequest(): void {
    setTimeout((): void => {
        const test = requestPromise({
            body: {
                count: 1,
            },
            json: true,
            method: "POST",
            uri: "http://127.0.0.1:27015/track",
        }).then((parsedBody): void => {
            console.log("Požadavek úspěšně odeslán.");
        }).catch((error): void => {
            console.log("Chyba odeslání požadavku.");
        });
    }, 5000);
}