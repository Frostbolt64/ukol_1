import * as bodyParser from "body-parser";
import * as express from "express";
import {CountController} from "./controllers/count.controller";
import {ErrorController} from "./controllers/error.controller";
import {TrackController} from "./controllers/track.controller";

/**
 * Rozhraní reprezentující nastavení serveru.
 */
export interface IServerConfiguration {
    ip: string;
    port: number;
}

/**
 * Třída reprezentující server objektu Singleton.
 */
export class Server {
    /**
     * Vrátí instanci objektu Server.
     */
    public static getInstance(): Server {
        if (!Server.instance) {
            Server.instance = new Server();
        }
        return Server.instance;
    }

    private static instance: Server;
    private serverApp: express.Application;
    private serverConfiguration: IServerConfiguration;

    /**
     * Privátní konstruktor.
     */
    private constructor() {
        this.serverApp = express();
        this.serverConfiguration = {
            ip: "127.0.0.1",
            port: 27015,
        };
    }

    /**
     * Vrátí objekt reprezentující konfiguraci serveru.
     */
    public getServerConfiguration(): IServerConfiguration {
        return this.serverConfiguration;
    }

    /**
     * Nastaví objekt reprezentující konfiguraci serveru. Objekt musí být platný.
     * 
     * @param serverConfiguration objekt reprezentující nastavení serveru
     */
    public setServerConfiguration(serverConfiguration: IServerConfiguration): void {
        if (!serverConfiguration) {
            throw new Error("Je třeba upřesnit konfiguraci serveru.");
        }
        this.serverConfiguration = serverConfiguration;
    }

    /**
     * Spustí server.
     */
    public runServer(): void {
        this.serverApp.use(bodyParser.json());
        this.serverApp.use(bodyParser.urlencoded({extended: false}));
        this.initRoutes();
        this.serverApp.listen(this.serverConfiguration.port, this.serverConfiguration.ip, (): void => {
            console.log("Server spuštěn na '" + this.serverConfiguration.ip + ":" + this.serverConfiguration.port + "'");
        });
    }

    /**
     * Inicializuje výchozí routery v celé aplikaci a jejich kontrolery.
     */
    private initRoutes(): void {
        this.serverApp.use("/count", new CountController().getRoutes());
        this.serverApp.use("/track", new TrackController().getRoutes());
        this.serverApp.use("*", new ErrorController().getRoutes());
    }
}