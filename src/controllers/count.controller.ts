import {Request, Response, Router} from "express";
import * as redis from "redis";
import {Controller} from "./default.controller";

/**
 * Tento kontrolorer obsluhuje všechny endpointy na routeru /count.
 */
export class CountController extends Controller {
    /**
     * Vytvoří nový objekt kontroleru pro obsluhu endpointů na routeru /count.
     */
    public constructor() {
        super();
        this.initRoutes();
    }

    /**
     * Count endpoint. Tato metoda je nastavena na endpointu /count, metody HTTP GET.
     * Získá hodnotu klíče 'count' z databáze Redis (pokud je přítomen) a vrátí jej zpět.
     * Pokud není hodnota klíče 'count' přítomna, je vypsána výchozí hláška. Pokud dojde k chybě,
     * je vyhozena výjimka.
     * 
     * @param request HTTP požadavek
     * @param response HTTP odpověď
     */
    protected showCount(request: Request, response: Response): void {
        const redisClient: redis.RedisClient = redis.createClient();
        redisClient.on("error", (error: any): void => {
            throw Error(error);
        });
        redisClient.get("count", (err: Error, reply: string): void => {
            if (err) {
                throw err;
            }
            response.writeHead(200, {"Content-Type": "text/plain; charset=utf8"});
            if (reply) {
                response.write(reply);
            }
            else {
                response.write("Zatím není uložena hodnota pod tímto klíčem.");
            }
            response.end();
            redisClient.quit();
        });
    }
    
    /**
     * Provede inicializaci routerů v tomto kontroleru.
     */
    protected initRoutes(): void {
        this.routes.get("/", this.showCount);
    }
}