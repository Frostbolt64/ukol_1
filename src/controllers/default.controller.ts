import {Request, Response, Router} from "express";

/**
 * Výchozí abstraktní třída, z které jsou odvozeny všechny kontrolery v aplikaci.
 */
export abstract class Controller {
    protected routes: Router;

    /**
     * Vytvoří objekt obecného kontroleru.
     */
    public constructor() {
        this.routes = Router();
    }

    /**
     * Vrátí objekt reprezentující routery v rámci express Frameworku.
     */
    public getRoutes(): Router {
        return this.routes;
    }

    /**
     * Abstraktní metoda pro inicializaci routerů v jednotlivých kontrolerech.
     */
    protected abstract initRoutes(): void;
}