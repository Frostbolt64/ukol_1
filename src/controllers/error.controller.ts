import {Request, Response, Router} from "express";
import {Controller} from "./default.controller";

/**
 * Tento kontrolorer obsluhuje všechny endpointy na výchozím routeru.
 */
export class ErrorController extends Controller {
    /**
     * Vytvoří nový objekt kontroleru pro obsluhu endpointů na všech routerech v rámci celé aplikace.
     */
    public constructor() {
        super();
        this.initRoutes();
    }

    /**
     * Odešle HTTP 404 kód, pokud požadavek přijde na jiný router, než jsou ty, které jsou v aplikaci
     * definovány.
     * 
     * @param request HTTP požadavek 
     * @param response HTTP odpověď
     */
    protected errorNotFound(request: Request, response: Response): void {
        response.status(404);
        response.end();
    }
    
    /**
     * Provede inicializaci routerů v tomto kontroleru.
     */
    protected initRoutes(): void {
        this.routes.get("*", this.errorNotFound);
    }
}