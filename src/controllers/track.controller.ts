import {Request, Response, Router} from "express";
import * as fs from "fs";
import * as redis from "redis";
import {Controller} from "./default.controller";

/**
 * Tento kontroler obsluhuje všechny endpointy umístěné na routeru /track.
 */
export class TrackController extends Controller {
    private static fileName: string = "body_requests.txt";

    /**
     * Vytvoří nový objekt kontroleru pro obsluhu endpointů na routeru /track.
     */
    public constructor() {
        super();
        this.initRoutes();
    }

    /**
     * Track endpoint. Tato metoda je nastavena na restpointu /track, metody HTTP POST.
     * Získá body požadavek ve formátu JSON a uloží jej do souboru. Pokud soubor neexistuje, bude vytvořen.
     * Pokud se v požadavku nachází klíč 'count', bude jeho hodnota uložena pod stejným klíčem do
     * databáze Redis. Pokud dojde k chybě, bude vyhozena výjimka. Pokud klíč 'count' či jeho hodnota
     * není číslo, nebude hodnota s tímto klíčem do databáze Redis uložena.
     * 
     * @param request HTTP požadavek
     * @param response HTTP odpověď
     */
    protected track(request: Request, response: Response): void {
        const bodyAsJson = request.body;
        fs.appendFile(TrackController.fileName, JSON.stringify(request.body) + "\r\n", (err: NodeJS.ErrnoException): void => {
            if (err) {
                console.log("Ops, nelze provést zápis do souboru: " + TrackController.fileName + ", chyba: " + err.message);
                throw err; 
            }
        });
        if (("count" in bodyAsJson) && (!isNaN(Number(bodyAsJson.count)))) {
            const redisClient: redis.RedisClient = redis.createClient();
            redisClient.on("error", (error: any): void => {
                throw Error(error);
            });
            redisClient.get("count", (err: Error, reply: string): void => {
                if (err) {
                    throw err;
                }
                if (reply) {
                    const redisNewCountVal = Number(reply) + Number(bodyAsJson.count);
                    redisClient.set("count", String(redisNewCountVal));
                }
                else {
                    redisClient.set("count", String(bodyAsJson.count));
                }
                redisClient.quit();
            });
        }
        response.end();
    }
    
    /**
     * Provede inicializaci routerů v tomto kontroleru.
     */
    protected initRoutes(): void {
        this.routes.post("/", this.track);
    }
}