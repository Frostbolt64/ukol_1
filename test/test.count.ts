/**
 * Test pro endpoint /count
 */

import * as chai from "chai";
import chaiHttp = require("chai-http");
import {IServerConfiguration, Server} from "../src/server";

const expect: Chai.ExpectStatic = chai.expect;
const serverConfiguration: IServerConfiguration = Server.getInstance().getServerConfiguration();

chai.use(chaiHttp);

describe("Testovani routeru /count.", (): void => {
    it("Endpoint /count HTTP GET musi vratit 200 status kod.", (done: Mocha.Done): void => {
        chai.request(serverConfiguration.ip + ":" + serverConfiguration.port).get("/count").end((err, res): void => {
            expect(res.status).to.be.equal(200);
            done();
        });
    });
    it("Endpoint /count musi mit hlavicku text/plain; charset=utf-8", (done: Mocha.Done): void => {
        chai.request(serverConfiguration.ip + ":" + serverConfiguration.port).get("/count").end((err, res): void => {
            expect(res).to.has.header("Content-Type", "text/plain; charset=utf8");
            done();
        });
    });
    it("Endpoint /count musi vracet číslo nebo řetězec.", (done: Mocha.Done): void => {
        chai.request(serverConfiguration.ip + ":" + serverConfiguration.port).get("/count").end((err, res): void => {
            expect(res.text).to.be.an("string");
            expect(Number(res.text)).to.be.an("number");
            done();
        });
    });
});