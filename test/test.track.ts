/**
 * Test pro endpoint /track
 */

import * as chai from "chai";
import chaiHttp = require("chai-http");
import {IServerConfiguration, Server} from "../src/server";

const expect: Chai.ExpectStatic = chai.expect;
const serverConfiguration: IServerConfiguration = Server.getInstance().getServerConfiguration();

chai.use(chaiHttp);

describe("Testovani routeru /track.", (): void => {
    it("Endpoint /track HTTP POST musi vratit 200 status kod.", (done: Mocha.Done): void => {
        chai.request(serverConfiguration.ip + ":" + serverConfiguration.port).post("/track").end((err, res): void => {
            expect(res.status).to.be.equal(200);
            done();
        });
    });
});