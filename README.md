# Úkol 1

Toto je první úkol zadání z: [http://www.ibillboard.com/uloha/vyvojar-uloha-1](http://www.ibillboard.com/uloha/vyvojar-uloha-1)

# Specifikace

Aplikace je vytvořena v typovém jazyce TypeScript na platformě Node.js

# Požadavky

1. Node.js
2. Redis (memory cache databáze)

- Node.js lze stáhnout zde: [https://nodejs.org/en/](https://nodejs.org/en/)
  Node.js je platforma umožňující běh JavaScriptu na straně serveru, která navíc obsahuje Npm (Node Package Manager) pro správu balíčků pro daný projekt.

- Redis lze stáhnout zde: [https://redis.io](https://redis.io)
  Jelikož jsem se s touto databází setkal poprvé, zjistil jsem, že se jedná o memory cache databázi, která funguje jako keška v reálném čase
  a je efektivní pro rychlé ukládání a vyvolávání dat (RAM). Byl jsem vcelku překvapen, jak jednoduše se data ukládají (jako do hešovací mapy).
 
Operační systém není upřesněn. Takže je jedno, zdali je použit pro provoz OS Windows, Linux, Unix. Prakticky je třeba dodržet výše uvedené požadavky.
Já vývoj řešil na OS Windows. Což ale neznamená, že nemám rád Linux a Linuxáky nebo Unix a Unixáky - to ne ;).

# Stažení repozitáře

Projekt lze stáhnout z tohoto repozitáře klasickým GIT příkazem:

```
git clone https://Frostbolt64@bitbucket.org/Frostbolt64/ukol_1.git
```

# Sestavení

# a) Závislosti

Jakmile je repozitář stažen, nejrychlejší cesta je otevřít CMD (terminál) a přepnout se do kořenového adresáře
projektu (záleží, jaký máte OS nebo zdali používáte nějaké IDE k vývoji).

Jakmile je CMD (terminál) otevřen ve správném adresáři s projektem, použijte příkaz:

```
npm install
```

a poté příkaz:

```
npm update
```

dojde ke stažení všech balíčků závislostí pro daný projekt pomocí Npm.

# b) Aplikace

Sestavení aplikace je jednoduché. Jakmile jsou vyřešeny závislosti, stačí spustit připravený skript následujícím příkazem:

```
npm run buildApp
```

Dojde k sestavení aplikace - překladu TypeScript souborů do JavaScript souborů.

**Tímto je aplikace připravena ke spuštění. Než ale aplikaci spustíte, je nutno spustit nejprve databázi Redis.**

# c) Testy

Sestavení testů je jednoduché. Jakmile jsou vyřešeny závislosti, stačí spustit připravený skript následujícím příkazem:

```
npm run buildTest
```

# Redis databáze

Popis, jak databázi Redis spustit, je zde: [https://redis.io/download](https://redis.io/download)

Ať už stáhnete verzi pro OS Windows nebo verzi pro OS Linux či Unix, je nutno spustit proces **redis-server**, který
spustí samotnou databázi na standardním síťovém rozhraní. Není třeba žádná konfigurace. Pouze spustit. Jakmile je databáze
Redis spuštěna a připravena příjimat požadavky, je možno spustit aplikaci.

**Redis databáze musí být spuštěna před spuštěním samotné aplikace! Jinak se aplikace nespustí a zahlásí chybu, že se není možné
k databázi Redis připojit.**

# Spuštění aplikace

Jakmile Redis databáze běží, samotnou aplikaci spustíte příkazem:

```
npm run start
```

# Spuštění testů

**Testy se musí spustit pouze, jakmile je spuštěna samotná aplikace!**

Testy se spustí příkazem:

```
npm run test
```

*Pro lepší režii testů lze využít také balíček ts-node, který přímo spustí soubor TypeScriptu. To jsem ale neřešil.*

# Závěr

Aplikace funguje a je otestována. Všechny stanovené testy prošly. Samozřejmě jsem se snažil, jak nejvíce to šlo. Při psaní jsem využil
také linter pro TypeScript, ať se snažím držet jakéhosi standardu pro stylyzaci psaného kódu.